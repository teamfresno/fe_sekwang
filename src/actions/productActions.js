import {
  GET_PRODUCTS_METHOD,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_FAILURE,
  GET_PRODUCT_INFO_METHOD,
  GET_PRODUCT_INFO_SUCCESS,
  GET_PRODUCT_INFO_FAILURE,
  CHANGE_PRODUCT_QUANTITY
} from "./types";

import axios from "axios";
import { authSetup } from "./auth";

export const getProducts = () => {
  const { auth } = authSetup();

  return dispatch => {
    dispatch(getProductsMethod());
    return axios({
      method: "get",
      headers: auth.optional,
      url: "http://localhost:8000/api/products"
    })
      .then(res => {
        dispatch(getProductsSucess(res.data.products));
      })
      .catch(err => dispatch(getProductsFailure(err)));
  };
};

export const getProductsMethod = () => {
  return {
    type: GET_PRODUCTS_METHOD
  };
};

export const getProductsSucess = products => {
  return {
    type: GET_PRODUCTS_SUCCESS,
    products
  };
};

export const getProductsFailure = () => {
  return {
    type: GET_PRODUCTS_FAILURE
  };
};

export const getProductInfo = id => {
  const { auth } = authSetup();

  return dispatch => {
    dispatch(getProductInfoMethod());
    return axios({
      method: "get",
      headers: auth.optional,
      url: `http://localhost:8000/api/products/${id}`
    })
      .then(res => {
        console.log(res.data);
        dispatch(getProductInfoSucess(res.data.product));
      })
      .catch(err => dispatch(getProductInfoFailure(err)));
  };
};

export const getProductInfoMethod = () => {
  return {
    type: GET_PRODUCT_INFO_METHOD
  };
};

export const getProductInfoSucess = selectedProduct => {
  return {
    type: GET_PRODUCT_INFO_SUCCESS,
    selectedProduct
  };
};

export const getProductInfoFailure = () => {
  return {
    type: GET_PRODUCT_INFO_FAILURE
  };
};

export const changeProductQuantity = qt => {
  return {
    type: CHANGE_PRODUCT_QUANTITY,
    qt: qt
  };
};
