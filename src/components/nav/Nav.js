import React from "react";
import style from "./Nav.module.css";
import { Link } from "react-router-dom";

import * as api from "../../api/index";

class Nav extends React.Component {
  // handleLogin = e => {
  //   const { authenticateUser } = this.props;

  //   e.preventDefault();

  //   const formData = {
  //     email: e.target.email.value,
  //     password: e.target.password.value
  //   };
  //   api.users.login(formData, authenticateUser);
  // };

  // handleSignup = e => {
  //   const { authenticateUser } = this.props;

  //   e.preventDefault();

  //   const formData = {
  //     email: e.target.email.value,
  //     password: e.target.password.value
  //   };
  //   api.users.sign_up(formData, authenticateUser);
  // };

  // handleLogout = () => {
  //   const { logoutUser } = this.props;
  //   logoutUser();
  // };

  render() {
    const { navClick } = this.props;

    return (
      <div className={style.outerWrapper}>
        <div className={style.mainWrapper}>
          <div className={style.leftHalf}>
            <div className={style.linkContainer}>
              <Link to="/product" onClick={navClick}>
                Home
              </Link>
              <Link to="/about" onClick={navClick}>
                ALL
              </Link>
              <Link to="/faq" onClick={navClick}>
                BEST SELLERS
              </Link>
              <Link to="/product" onClick={navClick}>
                BUNDLE
              </Link>
              <Link to="/about" onClick={navClick}>
                SALE
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Nav;
