import {
  GET_CART_METHOD,
  GET_CART_SUCCESS,
  GET_CART_FAILURE,
  UPDATE_CART_METHOD,
  UPDATE_CART_SUCCESS,
  UPDATE_CART_FAILURE
} from "./types";

import axios from "axios";
import { authSetup } from "./auth";

export const getCart = () => {
  const { auth } = authSetup();
  return dispatch => {
    dispatch(getCartMethod());
    return axios({
      method: "get",
      headers: auth.required,
      url: "http://localhost:8000/api/transactions"
    })
      .then(res => {
        dispatch(getCartSucess(res.data.cart.items));
      })
      .catch(err => dispatch(getCartFailure(err)));
  };
};

export const getCartMethod = () => {
  return {
    type: GET_CART_METHOD
  };
};

export const getCartSucess = cart => {
  let cartTotal = cart.reduce(
    (acc, cur) => acc + cur.qt * cur.product_id.price,
    0
  );
  return {
    type: GET_CART_SUCCESS,
    cart,
    cartTotal
  };
};

export const getCartFailure = err => {
  return {
    type: GET_CART_FAILURE
  };
};

export const updateCart = product => {
  const { auth } = authSetup();
  return dispatch => {
    dispatch(updateCartMethod());
    return axios({
      method: "post",
      headers: auth.required,
      url: "http://localhost:8000/api/transactions/cart",
      data: {
        product
      }
    })
      .then(res => {
        dispatch(updateCartSuccess(res.data));
      })
      .catch(err => dispatch(updateCartFailure(err)));
  };
};

export const updateCartMethod = () => {
  return {
    type: UPDATE_CART_METHOD
  };
};

export const updateCartSuccess = cart => {
  return dispatch => dispatch(getCart());
};

export const updateCartFailure = err => {
  return {
    type: UPDATE_CART_FAILURE,
    err
  };
};

// const loadCart = (data, cb) => {
//   axios({
//     method: "get",
//     headers: auth.required,
//     url: "http://localhost:8000/api/transactions"
//   })
//     .then(res => {
//       return cb(res);
//     })
//     .catch(err => {
//       console.error(err);
//     });
// };

// const orderHistory = (data, cb) => {
//   axios({
//     method: "get",
//     headers: auth.required,
//     url: "http://localhost:8000/api/transactions/all"
//   })
//     .then(res => {
//       return cb(res);
//     })
//     .catch(err => {
//       console.error(err);
//     });
// };

// const addToCart = (data, cb) => {
//   axios({
//     method: "post",
//     headers: auth.required,
//     url: "http://localhost:8000/api/transactions/cart",
//     data: {
//       product: data
//     }
//   })
//     .then(res => {
//       return cb(res);
//     })
//     .catch(err => {
//       console.error(err);
//     });
// };

// const emptyCart = (data, cb) => {
//   axios({
//     method: "post",
//     headers: auth.required,
//     url: "http://localhost:8000/api/transactions//cart/delete"
//   })
//     .then(res => {
//       return cb(res);
//     })
//     .catch(err => {
//       console.error(err);
//     });
// };
