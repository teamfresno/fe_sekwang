import * as auth from "../auth";

const axios = require("axios");

const loadCart = (data, cb) => {
  axios({
    method: "get",
    headers: auth.required,
    url: "http://localhost:8000/api/transactions"
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

const orderHistory = (data, cb) => {
  axios({
    method: "get",
    headers: auth.required,
    url: "http://localhost:8000/api/transactions/all"
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

const addToCart = (data, cb) => {
  axios({
    method: "post",
    headers: auth.required,
    url: "http://localhost:8000/api/transactions/cart",
    data: {
      product: data
    }
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

const emptyCart = (data, cb) => {
  axios({
    method: "post",
    headers: auth.required,
    url: "http://localhost:8000/api/transactions//cart/delete"
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

export { loadCart, orderHistory, addToCart, emptyCart };
