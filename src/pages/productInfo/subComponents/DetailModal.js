import React from "react";
import style from "./DetailModal.module.css";
import ReactTable from "react-table";
import "react-table/react-table.css";

import infoIcon from "../../../assets/images/info.svg";
import closeIcon from "../../../assets/images/outline-close-24px.svg";
const data1 = [
  {
    header: "bread",
    info:
      "en sldkfj slefj slfk jsldfkjelf jsdlf jesl; fjsdlfj lsdkfj eilf kjsdfli eksjf liek jslef jself jselfk jseilf ksjefl ekjf silsef jeslf ejf islefk jeilf skjef eil ksjlef sjefld"
  }
];

export default class DetailModal extends React.Component {
  state = {
    modalOpen: false
  };
  toggleModal = () => {
    const { modalOpen } = this.state;
    this.setState({ modalOpen: !modalOpen });
  };
  render() {
    const { modalOpen } = this.state;
    return (
      <div>
        <button className={style.detailBtn} onClick={this.toggleModal}>
          상세정보
          <img className={style.icons} src={infoIcon} alt="infoIcon" />
        </button>
        {modalOpen && (
          <div className={style.outerModal}>
            <div className={style.innerModal}>
              <div>
                <button
                  className={style.modalCloseBtn}
                  onClick={this.toggleModal}
                >
                  <img
                    className={style.icons}
                    src={closeIcon}
                    alt="closeIcon"
                  />
                </button>
              </div>
              <ReactTable
                data={data1}
                columns={[
                  {
                    accessor: "header",
                    style: {
                      display: "-webkit-flex",
                      alignItems: "center",
                      justifyContent: "center"
                    }
                  },
                  {
                    accessor: "info",
                    style: {
                      whiteSpace: "unset",
                      textOverflow: "unset",
                      textAlign: "initial"
                    }
                  }
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
                showPagination={false}
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}
