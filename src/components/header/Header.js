import React from "react";
import style from "./Header.module.css";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Nav from "../nav/Nav";

const Header = ({ isAuthenticated }) => {
  return (
    <div className={style.mainWrapper}>
      {isAuthenticated ? (
        <div className={style.linkContainer}>
          <Link to="/myaccount">MY ACCOUNT</Link>
        </div>
      ) : (
        <div className={style.linkContainer}>
          <Link to="/login">Login</Link>
          <Link to="/signup">Sign up</Link>
        </div>
      )}
      <Link className={style.adLink} to="/">
        광고 문구
      </Link>
    </div>
  );
};

Header.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.app.isAuthenticated
});

export default connect(mapStateToProps)(Header);
