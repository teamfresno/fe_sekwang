import React from "react";
import style from "./Cart.module.css";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { getCart } from "../../actions/cartActions";

import CartItem from "./subComponents/CartItem";

class Cart extends React.Component {
  componentDidMount = () => {
    const { getCart } = this.props;
    getCart();
  };

  render() {
    const { cart, cartTotal } = this.props;
    return (
      <div className={style.mainWrapper}>
        <div className={style.cartWrapper}>
          <div className={style.header}>SHOPPING CART</div>
          <div className={style.tags}>
            <div className={style.productName}>상품</div>
            <div className={style.productQt}>수량</div>
            <div className={style.productPrice}>가격</div>
          </div>
          {cart.map(item => (
            <CartItem key={item.product_id._id} item={item} />
          ))}
          <div className={style.subTotal}>{cartTotal}00원</div>
          <div className={style.purchaseBtnContainer}>
            <button className={style.purchaseBtn}>구매하기</button>
          </div>
        </div>
      </div>
    );
  }
}

Cart.propTypes = {
  cart: PropTypes.array.isRequired,
  cartTotal: PropTypes.number.isRequired,
  getCart: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  cart: state.app.cart,
  cartTotal: state.app.cartTotal
});

export default connect(
  mapStateToProps,
  { getCart }
)(withRouter(Cart));
