import * as auth from "../auth";

const axios = require("axios");

const sign_up = (form, cb) => {
  axios({
    method: "post",
    headers: auth.optional,
    url: "http://localhost:8000/api/users",
    data: {
      user: form
    }
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

const login = (form, cb) => {
  axios({
    method: "post",
    headers: auth.optional,
    url: "http://localhost:8000/api/users/login",
    data: {
      user: form
    }
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

const currentUser = (id, cb) => {
  axios({
    method: "get",
    headers: auth.required,
    url: "http://localhost:8000/api/users/current",
    data: {
      payload: id
    }
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};
export { sign_up, login, currentUser };
