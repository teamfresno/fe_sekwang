const optional = { "Access-Control-Allow-Origin": "*" };

const required = {
  "Access-Control-Allow-Origin": "*",
  Authorization: ""
};

const setAuthToken = token => {
  if (token) {
    required.Authorization = `Token ${token}`;
  } else {
    required.Authorization = "";
  }
};

export { optional, required, setAuthToken };
