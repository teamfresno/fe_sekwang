import React from "react";
import style from "./MyAccount.module.css";
import MyAccountNav from "./subComponents/MyAccountNavBar";

export default function MyAccount() {
  return (
    <div className={style.mainWrapper}>
      <div className={style.header}>My Account</div>
      <div className={style.horzDivider} />
      <div className={style.recentOrderContainer}>
        <div className={style.headerSm}>최근 주문 정보</div>
        <div className={style.recentOrder}>some format</div>
      </div>
      <div className={style.vertDivider} />
      <div className={style.navContainer}>
        <div className={style.headerSm}>회원정보</div>
        <MyAccountNav />
      </div>
    </div>
  );
}
