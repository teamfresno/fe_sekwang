import React from "react";
import style from "./Footer.module.css";

import { Link } from "react-router-dom";

class Footer extends React.Component {
  render() {
    return (
      <div className={style.mainWrapper}>
        <div className={style.contentWrapper}>
          <div className={style.support}>
            <Link className={style.link} to="/">
              Contact
            </Link>
            <Link className={style.link} to="/">
              Terms of Use
            </Link>
            <Link className={style.link} to="/">
              Privacy
            </Link>
          </div>
          <div className={style.brand}>SEKWANG FOOD INC.</div>
          <div className={style.sns}>
            <div className={style.iconContainer}>
              <i className="fab fa-facebook-f" />
            </div>
            <div className={style.iconContainer}>
              <i className="fab fa-twitter" />
            </div>
            <div className={style.iconContainer}>
              <i className="fab fa-instagram" />
            </div>
            <div className={style.iconContainer}>
              <i className="far fa-envelope" />
            </div>
          </div>
          <div className={style.copyright}>
            © Copyright 2019. All rights reserved.
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
