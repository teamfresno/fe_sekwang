import React from "react";
import style from "./ProductItem.module.css";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";

const ProductItem = ({ products }) => {
  return products.map(product => (
    <Link className={style.mainWrapper} to={`/product/${product._id}`}>
      <div className={style.productImg}>img</div>
      <div className={style.nameWrapper}>
        <div className={style.name}>{product.name}</div>
        <div className={style.price}>{product.price}00원</div>
      </div>
    </Link>
  ));
};

ProductItem.propTypes = {
  products: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  products: state.app.products
});

export default connect(mapStateToProps)(withRouter(ProductItem));
