import React from "react";
import style from "./Login.module.css";
import PropTypes from "prop-types";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { authUser } from "../../actions/authActions";

const Login = ({ authUser, isAuthenticated }) => {
  return isAuthenticated ? (
    <Redirect to="/" />
  ) : (
    <div className={style.mainWrapper}>
      <div className={style.formHeader}>Login</div>
      <div className={style.formWrapper}>
        <form onSubmit={authUser}>
          <input
            className={style.inputField}
            type="email"
            name="email"
            placeholder="이메일"
          />
          <input
            className={style.inputField}
            type="password"
            name="password"
            placeholder="패스워드"
          />
          <input className={style.submitBtn} type="submit" value="로그인" />
        </form>
      </div>
    </div>
  );
};

Login.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.app.isAuthenticated
});

export default connect(
  mapStateToProps,
  { authUser }
)(withRouter(Login));
