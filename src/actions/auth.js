export const authSetup = () => {
  const token = localStorage.jwtToken;
  return {
    auth: {
      optional: {
        "Access-Control-Allow-Origin": "*"
      },
      required: {
        "Access-Control-Allow-Origin": "*",
        Authorization: `Token ${token}`
      }
    }
  };
};
