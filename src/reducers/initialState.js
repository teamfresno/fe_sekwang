export const initialState = {
  isAuthenticated: false,
  user: {},
  products: [],
  selectedProduct: {},
  cart: [],
  cartTotal: 0,
  isLoading: true,
  error: false,
  carouselData: [
    {
      img_url: "https://i.ytimg.com/vi/Cq-XpdlRkp4/maxresdefault.jpg",
      headline: "some random string",
      subcontent: "some longer random string with strings and strings"
    },
    {
      img_url: "https://t1.daumcdn.net/cfile/tistory/99E23B4D5C346CBB21",
      headline: "some random string",
      subcontent: "some longer random string with strings and strings"
    }
  ]
};
