import React from "react";
import style from "./ProductInfo.module.css";
import "react-table/react-table.css";
import Select from "react-select";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import {
  getProductInfo,
  changeProductQuantity
} from "../../actions/productActions";

import { updateCart } from "../../actions/cartActions";

import DetailModal from "./subComponents/DetailModal";

class ProductInfo extends React.Component {
  componentDidMount = () => {
    const { getProductInfo, match } = this.props;
    getProductInfo(match.params.id);
  };

  handleAddBtn = () => {
    const { updateCart, selectedProduct } = this.props;
    const product = {
      product_id: selectedProduct._id,
      qt: selectedProduct.qt.value
    };
    updateCart(product);
  };

  render() {
    const { selectedProduct, changeProductQuantity } = this.props;
    return (
      <div className={style.mainWrapper}>
        <div className={style.mainContainer}>
          <div className={style.imgContainer}>
            <img
              className={style.productImg}
              src={selectedProduct.img_url}
              alt="product_img"
            />
          </div>
          <div className={style.infoContainer}>
            <div className={style.nameContainer}>{selectedProduct.name}</div>
            <div className={style.priceContainer}>
              {selectedProduct.price}00원
            </div>
            <div className={style.detailContainer}>
              Praesent laoreet ornare ligula, ac accumsan turpis sagittis at.
              Integer auctor egestas eleifend. Etiam luctus mattis justo, vitae
              fermentum libero euismod lacinia. Proin at consequat risus.
              Praesent sollicitudin vestibulum felis, ut sodales enim.
            </div>
            <DetailModal />
            <div className={style.qtContainer}>
              수량:
              <div className={style.qtInputWrapper}>
                <Select
                  options={[
                    { value: 1, label: 1 },
                    { value: 2, label: 2 },
                    { value: 3, label: 3 },
                    { value: 4, label: 4 },
                    { value: 5, label: 5 },
                    { value: 6, label: 6 },
                    { value: 7, label: 7 },
                    { value: 8, label: 8 },
                    { value: 9, label: 9 },
                    { value: "custom", label: "9+" }
                  ]}
                  onChange={changeProductQuantity}
                  value={selectedProduct.qt}
                />
              </div>
            </div>
            <div className={style.btnContainer}>
              <button className={style.addCartBtn} onClick={this.handleAddBtn}>
                장바구니 담기
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProductInfo.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  selectedProduct: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.app.isAuthenticated,
  selectedProduct: state.app.selectedProduct
});

export default connect(
  mapStateToProps,
  { getProductInfo, changeProductQuantity, updateCart }
)(withRouter(ProductInfo));
