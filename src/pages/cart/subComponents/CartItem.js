import React from "react";
import style from "./CartItem.module.css";
import Select from "react-select";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";

import { updateCart } from "../../../actions/cartActions";

import closeIcon from "../../../assets/images/outline-close-24px.svg";

class CartItem extends React.Component {
  handleChange = e => {
    const { updateCart, item } = this.props;
    const product = {
      product_id: item.product_id._id,
      qt: e.value
    };
    updateCart(product);
  };

  render() {
    const { item } = this.props;

    return (
      <div key={item.product_id._id} className={style.mainWrapper}>
        <div className={style.left}>
          <button className={style.deleteBtnContainer}>
            <img className={style.deleteBtn} src={closeIcon} alt="closeIcon" />
          </button>
          <div className={style.imgContainer}>
            <img
              className={style.productImgSm}
              src={item.product_id.img_url}
              alt="product_img"
            />
          </div>
          <div className={style.nameContainer}>
            <Link to={`/product/${item.product_id._id}`}>
              {item.product_id.name}
            </Link>
          </div>
        </div>
        <div className={style.qtContainer}>
          <Select
            options={[
              { value: 0, label: 0 },
              { value: 1, label: 1 },
              { value: 2, label: 2 },
              { value: 3, label: 3 },
              { value: 4, label: 4 },
              { value: 5, label: 5 },
              { value: 6, label: 6 },
              { value: 7, label: 7 },
              { value: 8, label: 8 },
              { value: 9, label: 9 },
              { value: "custom", label: "9+" }
            ]}
            onChange={this.handleChange}
            value={{ value: item.qt, label: item.qt }}
          />
        </div>
        <div className={style.priceContainer}>
          {item.product_id.price * item.qt}00원
        </div>
      </div>
    );
  }
}

CartItem.propTypes = {
  cart: PropTypes.array.isRequired,
  updateCart: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  cart: state.app.cart
});

export default connect(
  mapStateToProps,
  { updateCart }
)(withRouter(CartItem));
