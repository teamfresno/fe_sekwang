import React from "react";
import style from "./Signup.module.css";
import PropTypes from "prop-types";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { newUser } from "../../actions/authActions";

const Signup = ({ newUser, isAuthenticated }) => {
  return isAuthenticated ? (
    <Redirect to="/" />
  ) : (
    <div className={style.mainWrapper}>
      <div className={style.formHeader}>Sign Up</div>
      <div className={style.formWrapper}>
        <form onSubmit={newUser}>
          <input
            className={style.inputField}
            type="email"
            name="email"
            placeholder="이메일"
          />
          <input
            className={style.inputField}
            type="password"
            name="password"
            placeholder="패스워드"
          />
          <input className={style.submitBtn} type="submit" value="회원가입" />
        </form>
      </div>
    </div>
  );
};

Signup.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.app.isAuthenticated
});

export default connect(
  mapStateToProps,
  { newUser }
)(withRouter(Signup));
