import * as auth from "../auth";

const axios = require("axios");

const getAll = cb => {
  axios({
    method: "get",
    headers: auth.optional,
    url: "http://localhost:8000/api/products"
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

const getOne = (id, cb) => {
  axios({
    method: "get",
    headers: auth.optional,
    url: `http://localhost:8000/api/products/${id}`
  })
    .then(res => {
      return cb(res);
    })
    .catch(err => {
      console.error(err);
    });
};

export { getAll, getOne };
