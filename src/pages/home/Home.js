import React from "react";
import style from "./Home.module.css";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

const Home = ({ carouselData }) => {
  return (
    <div className={style.mainWrapper}>
      <div className={style.carouselWrapper}>
        <Carousel
          showArrows={false}
          dynamicHeight={true}
          infiniteLoop={true}
          autoPlay={true}
          showStatus={false}
          showThumbs={false}
          interval={5000}
        >
          {carouselData.map(item => (
            <div key={item.img_url}>
              <img src={item.img_url} alt="carouselImg" />
            </div>
          ))}
        </Carousel>
      </div>
      <div className={style.cardContainer}>
        <Link className={style.longCard}>Link to featured item</Link>
        <div className={style.space} />
        <Link className={style.shortCard}>Link to featured item</Link>
        <Link className={style.shortCard}>Link to featured item</Link>
        <div className={style.space} />
        <Link className={style.longCard}>Link to featured item</Link>
      </div>
      <div className={style.backPhoto}>asdfa</div>
      <div className={style.floatingCardWrapper}>
        <div className={style.floatingCard}></div>
      </div>
      <div className={style.banner}>
        <div className={style.bannerDesc}>some info</div>
      </div>
    </div>
  );
};

Home.propTypes = {
  carouselData: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  carouselData: state.app.carouselData
});

export default connect(mapStateToProps)(withRouter(Home));
