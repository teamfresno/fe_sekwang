import React from "react";
import style from "./Product.module.css";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { getProducts } from "../../actions/productActions";

import ProductItem from "./subComponents/ProductItem";

class Product extends React.Component {
  componentDidMount = () => {
    const { getProducts } = this.props;
    getProducts();
  };

  render() {
    return (
      <div className={style.mainWrapper}>
        <div className={style.bannerWrapper}>
          <div className={style.bannerDesc}>
            <h1>찬미를 사는가</h1>
            작고 새 같은 있으랴? 하는 주며, 용감하고 청춘의 그것은 방황하였으며,
            것이다.
          </div>
        </div>
        <div className={style.productContainer}>
          <ProductItem />
        </div>
      </div>
    );
  }
}

Product.propTypes = {
  carouselData: PropTypes.array.isRequired,
  getProducts: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  carouselData: state.app.carouselData
});

export default connect(
  mapStateToProps,
  { getProducts }
)(withRouter(Product));
