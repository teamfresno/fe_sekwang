import {
  AUTH_USER_METHOD,
  AUTH_USER_SUCCESS,
  AUTH_USER_FAILURE,
  NEW_USER_METHOD,
  NEW_USER_SUCCESS,
  NEW_USER_FAILURE,
  GET_USER_DATA_METHOD,
  GET_USER_DATA_SUCCESS,
  GET_USER_DATA_FAILURE,
  GET_PRODUCTS_METHOD,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_FAILURE,
  GET_CART_METHOD,
  GET_CART_SUCCESS,
  GET_CART_FAILURE,
  GET_PRODUCT_INFO_METHOD,
  GET_PRODUCT_INFO_SUCCESS,
  GET_PRODUCT_INFO_FAILURE,
  CHANGE_PRODUCT_QUANTITY,
  STOP_LOADING
} from "../actions/types";

import { initialState } from "./initialState";

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_USER_METHOD:
      return {
        ...state,
        isLoading: true
      };
    case AUTH_USER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        isLoading: false
      };
    case AUTH_USER_FAILURE:
      return {
        ...state,
        error: true,
        isLoading: false
      };
    case NEW_USER_METHOD:
      return {
        ...state,
        isLoading: true
      };
    case NEW_USER_SUCCESS:
      return {
        ...state,
        isNEWenticated: true,
        isLoading: false
      };
    case NEW_USER_FAILURE:
      return {
        ...state,
        error: true,
        isLoading: false
      };
    case GET_USER_DATA_METHOD:
      return {
        ...state,
        isAuthenticated: true,
        isLoading: true
      };
    case GET_USER_DATA_SUCCESS:
      return {
        ...state,
        user: action.user,
        isLoading: false
      };
    case GET_USER_DATA_FAILURE:
      return {
        ...state,
        error: true,
        isLoading: false
      };
    case GET_PRODUCTS_METHOD:
      return {
        ...state,
        isLoading: true
      };
    case GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.products,
        isLoading: false
      };
    case GET_PRODUCTS_FAILURE:
      return {
        ...state,
        error: true,
        isLoading: false
      };
    case GET_CART_METHOD:
      return {
        ...state,
        isLoading: true
      };
    case GET_CART_SUCCESS:
      return {
        ...state,
        cart: action.cart,
        cartTotal: action.cartTotal,
        isLoading: false
      };
    case GET_CART_FAILURE:
      return {
        ...state,
        error: true,
        isLoading: false
      };
    case GET_PRODUCT_INFO_METHOD:
      return {
        ...state,
        isLoading: true
      };
    case GET_PRODUCT_INFO_SUCCESS:
      return {
        ...state,
        selectedProduct: {
          ...action.selectedProduct,
          qt: { value: 1, label: 1 }
        },
        isLoading: false
      };
    case GET_PRODUCT_INFO_FAILURE:
      return {
        ...state,
        error: true,
        isLoading: false
      };
    case CHANGE_PRODUCT_QUANTITY:
      return {
        ...state,
        selectedProduct: { ...state.selectedProduct, qt: action.qt }
      };
    case STOP_LOADING:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};
