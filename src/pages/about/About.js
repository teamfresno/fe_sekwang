import React from "react";
import style from "./About.module.css";

const About = () => {
  return (
    <div className={style.mainWrapper}>
      <div className={style.bannerWrapper}>
        <div className={style.preBanner}>자연 그대로</div>
        <div className={style.banner}>Lorem ipsum dolor sit amet</div>
        <div className={style.subBanner}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor
          voluptas harum aperiam minus corporis, <br /> nesciunt nobis quaerat
          voluptatumunt qui similique.
        </div>
      </div>
      <div className={style.frame}>
        <div className={style.frameItem1} />
        <div className={style.frameItem2} />
        <div className={style.frameItem3} />
        <div className={style.frameItem4} />
        <div className={style.frameItem5} />
        <div className={style.frameItem6} />
      </div>
      <div className={style.info}>
        <div className={style.infoImg1} />
        <div className={style.infoImg2} />
        <div className={style.header}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </div>
        <div className={style.subHeader}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor
          voluptas harum aperiam minus corporis, nesciunt nobis quaerat
          voluptatumunt qui similique. Lorem ipsum dolor sit amet, consectetur
          adipisicing elit. Dolor voluptas harum aperiam minus corporis,
          nesciunt nobis quaerat voluptatumunt qui similique.
        </div>
      </div>
      <div className={style.processWrapper}>
        <div className={style.processBanner}>Lorem ipsum dolor sit amet</div>
        <div className={style.process1}>
          <i className="fas fa-border-all fa-2x" />
          <div className={style.processHeader}>Lorem ipsum dolor</div>
          <div className={style.processBody}>
            Lorem ipsum dolor sit ametLorem ipsum dolor sit ametvoluptatumunt
            qui similique. Lorem ipsum dolor sit amet, consectetur adipisicing
            elit. Dolor voluptas harum aperiam minus corporis, nesciunt nobis
            quaerat voluptatumunt qui similique.
          </div>
        </div>
        <div className={style.process2}>
          <i className="fas fa-history fa-2x" />
          <div className={style.processHeader}>Lorem ipsum dolor</div>
          <div className={style.processBody}>
            Lorem ipsum dolor sit ametLorem ipsum dolor sit ametvoluptatumunt
            qui similique. Lorem ipsum dolor sit amet, consectetur adipisicing
            elit. Dolor voluptas harum aperiam minus corporis, nesciunt nobis
            quaerat voluptatumunt qui similique.
          </div>
        </div>
        <div className={style.process3}>
          <i className="fas fa-globe fa-2x" />
          <div className={style.processHeader}>Lorem ipsum dolor</div>
          <div className={style.processBody}>
            Lorem ipsum dolor sit ametLorem ipsum dolor sit ametvoluptatumunt
            qui similique. Lorem ipsum dolor sit amet, consectetur adipisicing
            elit. Dolor voluptas harum aperiam minus corporis, nesciunt nobis
            quaerat voluptatumunt qui similique.
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
