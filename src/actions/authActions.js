import {
  AUTH_USER_METHOD,
  AUTH_USER_SUCESS,
  AUTH_USER_FAILURE,
  NEW_USER_METHOD,
  NEW_USER_SUCESS,
  NEW_USER_FAILURE,
  GET_USER_DATA_METHOD,
  GET_USER_DATA_SUCCESS,
  GET_USER_DATA_FAILURE,
  STOP_LOADING
} from "./types";
import axios from "axios";
import { authSetup } from "./auth";

export const authUser = e => {
  e.preventDefault();
  const { auth } = authSetup();
  const form = {
    email: e.target.email.value,
    password: e.target.password.value
  };
  return dispatch => {
    dispatch(authUserMethod());
    return axios({
      method: "post",
      headers: auth.optional,
      url: "http://localhost:8000/api/users/login",
      data: {
        user: form
      }
    })
      .then(res => {
        dispatch(authUserSucess(res.data.user));
      })
      .catch(err => dispatch(authUserFailure(err)));
  };
};

export const authUserMethod = () => {
  return {
    type: AUTH_USER_METHOD
  };
};

export const authUserSucess = user => {
  localStorage.setItem("jwtToken", user.token);
  return dispatch => dispatch(getUserData());
};

export const authUserFailure = () => {
  return {
    type: AUTH_USER_FAILURE
  };
};

export const getUserData = () => {
  const { auth } = authSetup();
  return dispatch => {
    dispatch(getUserDataMethod());
    return axios({
      method: "get",
      headers: auth.required,
      url: "http://localhost:8000/api/users/current"
    })
      .then(res => {
        dispatch(getUserDataSuccess(res.data));
      })
      .catch(err => dispatch(getUserDataFailure(err)));
  };
};

export const getUserDataMethod = () => {
  return {
    type: GET_USER_DATA_METHOD
  };
};

export const getUserDataSuccess = user => {
  return {
    type: GET_USER_DATA_SUCCESS,
    user
  };
};

export const getUserDataFailure = err => {
  return {
    type: GET_USER_DATA_FAILURE,
    err
  };
};

export const newUser = e => {
  const { auth } = authSetup();
  const form = {
    email: e.target.email.value,
    password: e.target.password.value
  };
  return dispatch => {
    return axios({
      method: "post",
      headers: auth.optional,
      url: "http://localhost:8000/api/users",
      data: {
        user: form
      }
    })
      .then(res => {
        console.log(res);
        dispatch(newUserSuccess(res.data.user));
      })
      .catch(err => dispatch(newUserFailure(err)));
  };
};

export const newUserMethod = () => {
  return {
    type: NEW_USER_METHOD
  };
};

export const newUserSuccess = user => {
  localStorage.setItem("jwtToken", user.token);
  return dispatch => dispatch(getUserData());
};

export const newUserFailure = err => {
  return {
    type: NEW_USER_FAILURE,
    err
  };
};

export const stopLoading = () => {
  return {
    type: STOP_LOADING
  };
};
