import React from "react";
import style from "./ProductGroup.module.css";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import ProductItem from "./ProductItem";

const ProductGroup = ({products}) => {
    return (
      <div className={style.mainWrapper}>
        {products.map(product => (
          <ProductItem product={product} key={product._id} />
        ))}
      </div>
    );
}

ProductGroup.propTypes = {
  carouselData: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  carouselData: state.app.carouselData
});

export default connect(mapStateToProps)(withRouter(ProductGroup));
