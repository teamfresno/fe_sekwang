import React from "react";
import style from "./SubHeader.module.css";
import { Link } from "react-router-dom";

import Nav from "../nav/Nav";

import menuIcon from "../../assets/images/outline-menu-24px.svg";
// import closeIcon from "../../assets/images/outline-close-24px.svg";
// import cartIcon from "../../assets/images/outline-shopping_cart-24px.svg";

class SubHeader extends React.Component {
  state = {
    sticky: false,
    innerWidth: "",
    isMobile: false,
    nav: false
  };

  componentDidMount = () => {
    window.addEventListener("scroll", this.handleScroll);
    // const { expandNav } = this.props;
    // if (expandNav) this.setState({ nav: true });
    const { innerWidth } = window;
    this.setState({ innerWidth });
    innerWidth <= 768 && this.setState({ isMobile: true });
  };

  componentWillUnmount = () => {
    window.removeEventListener("scroll", this.handleScroll);
  };

  navClick = () => {
    const { nav } = this.state;

    this.setState({ nav: !nav });
  };

  handleScroll = () => {
    const { scrollY } = window;
    // let scrollTop = e.srcElement.body.scrollTop,
    //   itemTranslate = Math.min(0, scrollTop / 3 - 60);
    scrollY >= 35
      ? this.setState({ sticky: true })
      : this.setState({ sticky: false });
    // this.setState({
    //   transform: itemTranslate
    // });
  };

  render() {
    const { sticky, innerWidth, isMobile, nav } = this.state;
    const { isAuthenticated } = this.props;
    const headerStyle = sticky ? style.sticky : style.regular;
    const navStyle = isMobile ? style.navMobile : style.navDesktop;
    return (
      <div className={headerStyle}>
        <Link className={style.brand} to="/">
          S E K W A N G
        </Link>
        <div className={navStyle}>
          {isMobile && (
            <button className={style.icons} onClick={this.navClick}>
              <img src={menuIcon} alt="hamburger" />
            </button>
          )}
          {!isMobile && (
            <div className={style.linkContainer}>
              <Link className={style.navLink} to="/product">
                SHOP
              </Link>
              <Link className={style.navLink} to="/about">
                ABOUT
              </Link>
              <Link className={style.navLink} to="/process">
                PROCESS
              </Link>
              <Link className={style.navLink} to="/contact">
                CONTACT
              </Link>
              <Link className={style.navLink} to="/cart">
                CART
              </Link>
            </div>
          )}
          {nav && <Nav navClick={this.navClick} />}
        </div>
        {/* {nav && <Nav navClick={this.navClick} />} */}
      </div>
    );
  }
}

export default SubHeader;
