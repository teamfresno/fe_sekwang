import React from "react";
import style from "./MyAccountNavBar.module.css";
import { Link } from "react-router-dom";
export default function MyAccountNavBar() {
  return (
    <div className={styleMedia.mainWrapper}>
      <Link className={style.link} to="/">
        주문/배송 조회
      </Link>
      <Link className={style.link} to="/">
        회원정보 변경
      </Link>
      <Link className={style.link} to="/">
        배송지 관리
      </Link>
      <Link className={style.link} to="/">
        1:1 문의
      </Link>
      <Link className={style.link} to="/">
        FAQ
      </Link>
    </div>
  );
}
