import React from "react";
import { Route, withRouter } from "react-router-dom";
import style from "./App.module.css";
import BounceLoader from "react-spinners/BounceLoader";
import PropTypes from "prop-types";
import { css } from "@emotion/core";
import jwt_decode from "jwt-decode";
import { connect } from "react-redux";

import { getUserData, stopLoading } from "./actions/authActions";

import Home from "./pages/home/Home";
import About from "./pages/about/About";
import Product from "./pages/product/Product";
import ProductInfo from "./pages/productInfo/ProductInfo";
import Cart from "./pages/cart/Cart";
import Login from "./pages/login/Login";
import Signup from "./pages/signup/Signup";
import MyAccount from "./pages/myaccount/MyAccount";

// route utils
import PrivateRoute from "./routes/PrivateRoute";

// components
import Header from "./components/header/Header";
import SubHeader from "./components/subHeader/SubHeader";
import Footer from "./components/footer/Footer";

// APIs
import * as api from "./api";

class App extends React.Component {
  componentDidMount() {
    // const { isAuthenticated } = this.state;
    // if (!isAuthenticated) {
    //   if (localStorage.jwtToken) {
    //     api.auth.setAuthToken(localStorage.jwtToken);
    //     const decoded = jwt_decode(localStorage.jwtToken);
    //     const { userId } = decoded;
    //     this.setState({ loading: true });
    //     // setTimeout(() => {
    //     api.users.currentUser(userId, this.getCurrentUserData);
    //     // }, 5000);
    //     // store.dispatch(setCurrentUser(decoded));
    //     // const currentTime = Date.now() / 1000;
    //     // if (decoded.exp < currentTime) {
    //     //   store.dispatch(logoutUser());
    //     //   window.location.href = "/";
    //     // }
    //   }
    // }
    // this.fetchProductData();
    const { getUserData, stopLoading } = this.props;
    const token = localStorage.jwtToken;
    if (token) getUserData();
    else stopLoading();
  }
  render() {
    const { isLoading } = this.props;
    const override = css`
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    `;

    return (
      <div className="App">
        {/* loading indicator */}
        {isLoading && (
          <div className={style.loadingOuterModal}>
            <BounceLoader css={override} />
          </div>
        )}
        <Header />
        <SubHeader />
        {/* public routes */}
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/product" component={Product} />
        <Route exact path="/product/:id" component={ProductInfo} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Signup} />
        {/* private routes */}
        <PrivateRoute exact path="/cart" component={Cart} />
        <PrivateRoute exact path="/myaccount" component={MyAccount} />
        <Footer />
      </div>
    );
  }
}

App.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  cart: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.app.isAuthenticated,
  user: state.app.user,
  isLoading: state.app.isLoading,
  cart: state.app.cart
});

export default connect(
  mapStateToProps,
  { getUserData, stopLoading }
)(withRouter(App));
